<?php
ini_set('max_execution_time', '0');
echo 'File parsing is started. Please wait.....' . PHP_EOL;

$filename = $argv[2];
$output_filename = $argv[3];
if(strpos($argv[3], '=') !== false){
  $op_arr = explode('=', $argv[3]);
  $output_filename = $op_arr[1];
}

$rows = _csv_row_count($filename);
// print_r($rows); exit;

$head_arr = array('make', 'model', 'colour', 'capacity', 'network', 'grade', 'condition', 'count');
$fp = fopen($output_filename, 'w');
fputcsv($fp, $head_arr);
fclose($fp);

$items_per_run = 1000;
for ($i=0; $i <= $rows; $i = $i+$items_per_run+1) {
  $chunk = _csv_slice($filename, $i, $items_per_run);
  $category_count = array();
  foreach ($chunk as $item) {
  	if(isset($category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name])){
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['count'] = $category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['count'] + 1;
  	}else{
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name] = array();
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['make'] = $item->brand_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['model'] = $item->model_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['colour'] = $item->colour_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['capacity'] = $item->gb_spec_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['network'] = $item->network_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['grade'] = $item->grade_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['condition'] = $item->condition_name;
  		$category_count[$item->brand_name . '_' . $item->model_name . '_' . $item->grade_name . '_' . $item->gb_spec_name . '_' . $item->colour_name . '_' . $item->network_name]['count'] = 1;
  	}

    // echo "$i - item category = " .  $item->brand_name . "<br>"; //Note CurrentURL is a case sensitive  
  }
}
// echo '<pre>';
// print_r($category_count);

$fp = fopen($output_filename, 'a+');
foreach ($category_count as $key => $val) {
  echo implode('|', $val) . PHP_EOL;
  fputcsv($fp, $val);
}
fclose($fp);


function _csv_row_count($filename) {
  ini_set('auto_detect_line_endings', TRUE);
  $row_count = 0;
  if (($handle = fopen($filename, "r")) !== FALSE) {
    while (($row_data = fgetcsv($handle, 2000, ",")) !== FALSE) {
      $row_count++;
    }
    fclose($handle);
    // Exclude the headings.
    $row_count--;
    return $row_count;
  }
}


function _csv_slice($filename, $start, $desired_count) {
  $row = 0;
  $count = 0;
  $rows = array();
  if (($handle = fopen($filename, "r")) === FALSE) {
    return FALSE;
  }
  while (($row_data = fgetcsv($handle, 2000, ",")) !== FALSE) {
    // Grab headings.
    if ($row == 0) {
      $headings = $row_data;
      $row++;
      continue;
    }

    // Not there yet.
    if ($row++ < $start) {
      continue;
    }

    $rows[] = (object) array_combine($headings, $row_data);
    $count++;
    if ($count == $desired_count) {
      return $rows;
    }
  }
  return $rows;
}